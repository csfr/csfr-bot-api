const { spawn } = require('child_process');
const tmi = require('tmi.js');
const client = new tmi.Client({
    options: {debug: true},
    connection: {
        reconnect: true,
        secure: true,
    },
    /* (TODO [ ]) alpha1
    [ ] Get OAuth token off-stream.
    [ ] To be loaded in 'config.js'
    [ ] To chmod read only for user
    identity: {
        username: '',
        password: 'oauth:token',
    }
    */ 
   channels: ['codeskunkfr']
})

client.connect();

let isAboutTwitch = (message) => {
    if (message.match(/twitch.tv/)) {
        console.log(`Match!!! ${message}`)
        let m = message.match(/\/(.*)$/)
        console.log(m)
        if (m && m[1]) {
            return `https://twitch.tv/${message.split('/').slice(-1)}`
        }
    }
}

/* isAboutYoutube search for youtube related intents */
let isAboutYoutube = (message) => {
    if (message.match(/youtube\.com/)) {
        console.log(`Match!!! ${message}`)
        let m = message.match(/v=(.*)$/)
        if (m && m[1]) {
            return `https://youtube.com/watch?v=${m[1]}`
        }
    }
    return false
}

let playVideo = (options) => {
    let args = ''
    if (options) {
        if (options.muted) {
            args += '--audio=no'
        }
        if (!options.id) {
            console.log('Warning: Id Unavailable not spawning mpv.')
            return
        }
    }
    console.log(`playerVideo:Info: ${args}`)
    let player = spawn('mpv', [args, `${options.id}`])
    console.log(`Info: player started
    (suggested by ${options['tags'].username})`)
    player.on('close', (code) => {
        console.log(`Info: player closed with code ${code}`)
    })
}

let playVideoMuted = (url, options) => playVideo({
    id: url,
    muted: true,
    tags: options.tags,
})
let playVideoWithSound = (url, options) => playVideo({
    id: url,
    tags: options.tags,
})

let sayId = (id) => {
    console.log(`Id is ${id}`)
}

/* expected parameter for intents: raw message*/
let intents = {
    'Youtube': [isAboutYoutube],
    'Twitch': [isAboutTwitch],
}

/* expected a first paramater for actions: raw text (hash) 
   and also a second paramater : options (see action definitions) */
let actions = {
    'Youtube': [playVideoWithSound, sayId],
    'Twitch': [playVideoMuted],
}

let main = (client) => {
    client.on('message', (channel, tags, message, self) => {
        // Object.keys(tags).forEach(tag => {
        //     console.log(`${tag}: ${tags[tag]}`)
        // })
        // Plugins Here
        let intentsTargets = Object.keys(intents)
        let answers = {}
        for (let i = 0; i < intentsTargets.length; i++) {
           const target = intentsTargets[i]
           console.log(target)
           for (let k = 0; k < intents[target].length; k++) {
               answers[target] = intents[target][k](message)
           }
        }
        let intentsActions = Object.keys(answers)
        for (let i = 0; i < intentsActions.length; i++) {
            const action = intentsActions[i]
            if (!answers[action]) {
                continue
            }
            console.log('Debug:', action, actions[action])
            for (let k = 0; k < actions[action].length; k++) {
                const hash = answers[action]
                console.log(`Info: Spawning action ${k}`)
                actions[action][k](hash, {tags: tags})
            }
        } 
        console.log(`Robots ANSWSERS: ${JSON.stringify(answers, null, 2)}`)
        // End of Plugins 
        console.log('Info: done with intent scanning.')
        console.log(`${message}`)
    })
}

main(client)